from django.contrib import admin
from doctor.models import BaseContact, BaseSlider, DoctorBlog, Doctor, BlogTv, \
    Xestelik, Country, PaymentModel, SponsorModel, MeetingModels, Banner, BlogComment
from django.contrib.auth import get_user_model
User = get_user_model()

# Register your models here.
class BaseContactsAdmin(admin.ModelAdmin):
    list_display = ('phone', 'email', 'locations')
    search_fields = ('email',)



class DoctorAdmin(admin.ModelAdmin):
    readonly_fields = ('get_user_full_name','get_user_diplom','get_user_email')
    fields = ('get_user_full_name','get_user_diplom','get_user_email','tesdiq', 'payment_status','position','rate')

class PaymentAdmin(admin.ModelAdmin):
    list_display = ('user','reference','description','prize','status','base_date')

class SponsorAdmin(admin.ModelAdmin):
    list_display = ('doctor','place','prize','status','base_date')



admin.site.register(BaseContact, BaseContactsAdmin)
admin.site.register(BaseSlider)
admin.site.register(DoctorBlog)
admin.site.register(Xestelik)
admin.site.register(Country)
admin.site.register(Doctor, DoctorAdmin)
admin.site.register(BlogTv)
admin.site.register(PaymentModel, PaymentAdmin)
admin.site.register(SponsorModel, SponsorAdmin)
admin.site.register(MeetingModels)
admin.site.register(Banner)
admin.site.register(BlogComment)