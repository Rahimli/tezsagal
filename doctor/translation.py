from modeltranslation.translator import translator, TranslationOptions
from doctor.models import *


class DoctorTranslationOption(TranslationOptions):
    fields = ('pasient_types', 'work_place', 'job_description', 'description', 'education', 'goals')


translator.register(Doctor, DoctorTranslationOption)
